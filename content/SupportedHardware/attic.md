+++
title = "Attic"
weight = "16"
+++

- [memnic](http://dpdk.org/doc/memnic-pmd) (Qemu IVSHMEM)
- [vmxnet3 usermap](http://dpdk.org/doc/vmxnet3-usermap) (VMware ESXi without uio)
